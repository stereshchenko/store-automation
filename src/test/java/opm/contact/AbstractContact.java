package opm.contact;

public abstract class AbstractContact implements IContact {
    private final String firstName;
    private final String lastName;
    private final String address;
    private final String city;
    private final String region;
    private final String country;
    private final String postalCode;
    private final ContactType contactType;

    protected AbstractContact(ContactType contactType, String firstName, String lastName, String address, String city, String region,
                              String country, String postalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.city = city;
        this.region = region;
        this.country = country;
        this.postalCode = postalCode;
        this.contactType = contactType;
    }

    protected AbstractContact(ContactType type, IContact contact) {
        this(type, contact.getFirstName(), contact.getLastName(), contact.getAddress(), contact.getCity(),
                contact.getRegion(), contact.getCountry(), contact.getPostalCode());
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getAddress() {
        return this.address;
    }

    public String getCity() {
        return this.city;
    }

    public String getRegion() {
        return this.region;
    }

    public String getCountry() {
        return this.country;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public ContactType getContactType() {
        return this.contactType;
    }

}
