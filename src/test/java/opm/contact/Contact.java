package opm.contact;

public class Contact extends AbstractContact {
    public Contact(ContactType type, String firstName, String lastName, String address, String city, String region,
                   String country, String postalCode) {
        super(type, firstName, lastName, address, city, region, country, postalCode);
    }
}
