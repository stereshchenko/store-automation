package opm.contact;

public interface IContact {
    String getFirstName();

    String getLastName();

    String getAddress();

    String getCity();

    String getRegion();

    String getCountry();

    String getPostalCode();

    ContactType getContactType();
}