package opm.pages.checkout;

import opm.product.CheckOutProduct;
import opm.product.IProduct;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.Converter;
import utils.WebDriverHelper;

import java.util.ArrayList;
import java.util.List;

public class FinalPage extends CheckoutAbstractPage implements IPageWithProductInfo {

    protected final String productRowXpath = "//tr[descendant::td[text()='%s']]";
    protected final String valueXpath = "%s/td[%s]";
    protected final String productPriceXpath = String.format(valueXpath, productRowXpath, 2);
    protected final String productTotalPriceXpath = String.format(valueXpath, productRowXpath, 4);
    protected final String productQuantityXpath = String.format(valueXpath, productRowXpath, 3);
    @FindBy(xpath = "//tbody//td[1]")
    protected List<WebElement> productTitles;

    public FinalPage(WebDriver driver) {
        super(driver, "final");
    }

    public List<CheckOutProduct> getProducts() {
        List<CheckOutProduct> products = new ArrayList();

        getProductsTitles().forEach((title) -> products.add(getProduct(title)));
        return products;
    }

    public CheckOutProduct getProduct(String productTitle) {
        CheckOutProduct product = new CheckOutProduct(productTitle);
        return (CheckOutProduct) product.setQuantity(Integer.parseInt(getQuantityForProduct(product)))
                .setTotalPrice(Converter.priceToFloat(getTotalPriceForProduct(product)))
                .setCurrentPrice(getCurrentPriceForProduct(product));
    }

    private List<String> getProductsTitles() {
        List<String> productsTitles = new ArrayList();
        productTitles.forEach(a -> productsTitles.add(a.getText()));
        return productsTitles;
    }

    protected String getCurrentPriceForProduct(IProduct product) {
        return WebDriverHelper.findElementByXpath(driver, String.format(this.productPriceXpath, product.name()),
                this.ELEMENT_APPEAR_TIMEOUT).getText();
    }

    protected String getQuantityForProduct(IProduct product) {
        return WebDriverHelper.findElementByXpath(driver, String.format(this.productQuantityXpath, product.name()),
                this.ELEMENT_APPEAR_TIMEOUT).getText();
    }

    protected String getTotalPriceForProduct(IProduct product) {
        return WebDriverHelper.findElementByXpath(driver, String.format(this.productTotalPriceXpath, product.name()),
                this.ELEMENT_APPEAR_TIMEOUT).getText();
    }

}
