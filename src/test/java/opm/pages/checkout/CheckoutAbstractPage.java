package opm.pages.checkout;

import opm.pages.MainHeaderAbstractPage;
import org.openqa.selenium.WebDriver;
import utils.WebDriverHelper;

public abstract class CheckoutAbstractPage extends MainHeaderAbstractPage {
    protected final String checkoutPageXpath = "//div[@id='checkout_page_container']";
    protected final String selectedProgressBarTabXpath = "(//li[contains(@class, 'act')][contains(@class, '%s')][not(following-sibling::li[contains(@class, 'act')])])[1]";
    protected String tabID;

    protected CheckoutAbstractPage(WebDriver driver, String tabID) {
        super(driver);
        this.tabID = tabID;
    }

    public void waitForPage() {
        WebDriverHelper.findElementByXpath(driver, String.format(selectedProgressBarTabXpath, tabID), this.PAGE_LOADING_TIMEOUT);
    }
}
