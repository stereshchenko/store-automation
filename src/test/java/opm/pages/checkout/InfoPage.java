package opm.pages.checkout;

import opm.contact.BillingContact;
import opm.contact.ContactType;
import opm.contact.IContact;
import opm.contact.ShippingContact;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WebDriverHelper;

public class InfoPage extends CheckoutAbstractPage {

    protected final String tableInputXpath = "//tr//input[@placeholder='%s']";
    protected final String tableTextAreaXpath = "//tr//textarea[@placeholder='%s']";
    protected final String tableDropdownXpath = "//tr[child::td[contains(.,'%s')]]//select";
    protected final String dropdownOptionXpath = "/option[text()='%s']";
    private final String billingTableXpath = "//table[contains(@class,'table-1')]";
    private final String shippingTableXpath = "//table[contains(@class,'table-2')]";
    @FindBy(xpath = checkoutPageXpath + "//input[@placeholder='Email']")
    WebElement emailInput;

    @FindBy(xpath = checkoutPageXpath + "//input[@value='Purchase']")
    WebElement purchaseButton;

    public InfoPage(WebDriver driver) {
        super(driver, "info");
    }

    public InfoPage populateBillingContact(BillingContact contact) {
        return populateContact(contact).setPhone(contact.getPhone());
    }

    public InfoPage populateShippingContact(ShippingContact contact) {
        return populateContact(contact);
    }

    public InfoPage populateContacts(ShippingContact shippingContact, BillingContact billingContact) {
        return populateShippingContact(shippingContact).populateBillingContact(billingContact);
    }

    protected InfoPage populateContact(IContact contact) {
        ContactType contactType = contact.getContactType();
        return setFirstName(contact.getFirstName(), contactType).setLastName(contact.getLastName(), contactType)
                .setAddress(contact.getAddress(), contactType).setCity(contact.getCity(), contactType)
                .setRegion(contact.getRegion(), contactType).setPostalCode(contact.getPostalCode(), contactType)
                .setCountry(contact.getCountry(), contactType);
    }

    protected String getTableXpath(ContactType type) {
        if (type == ContactType.BILLING_CONTACT)
            return this.billingTableXpath;
        if (type == ContactType.SHIPPING_CONTACT)
            return this.shippingTableXpath;
        throw new IllegalArgumentException(String.format("Table Locator for %s ContactType is not implemented.", type.name()));
    }

    protected void setInputValueInContactTable(String title, String value, ContactType type) {
        WebElement element = WebDriverHelper.findElementByXpath(driver, String.format(getTableXpath(type) + tableInputXpath, title),
                this.ELEMENT_APPEAR_TIMEOUT);
        WebDriverHelper.waitForElementVisibility(driver, element, this.TIMEOUT);
        element.clear();
        element.sendKeys(value);
    }

    protected void setTextAreaValueInContactTable(String title, String value, ContactType type) {
        WebElement element = WebDriverHelper.findElementByXpath(driver, String.format(getTableXpath(type) + tableTextAreaXpath, title),
                this.ELEMENT_APPEAR_TIMEOUT);
        WebDriverHelper.waitForElementVisibility(driver, element, this.TIMEOUT);
        element.clear();
        element.sendKeys(value);
    }

    protected void selectDropdownValueInContactTable(String title, String value, ContactType type) {
        WebDriverHelper.findElementByXpath(driver, String.format(getTableXpath(type) + tableDropdownXpath, title),
                this.ELEMENT_APPEAR_TIMEOUT).click();
        WebDriverHelper.findElementByXpath(driver, String.format(getTableXpath(type) + tableDropdownXpath + dropdownOptionXpath, title, value),
                this.TIMEOUT).click();
    }

    public InfoPage setFirstName(String value, ContactType type) {
        setInputValueInContactTable("First Name", value, type);
        return this;
    }

    public InfoPage setLastName(String value, ContactType type) {
        setInputValueInContactTable("Last Name", value, type);
        return this;
    }

    public InfoPage setAddress(String value, ContactType type) {
        setTextAreaValueInContactTable("Address", value, type);
        return this;
    }

    public InfoPage setCity(String value, ContactType type) {
        setInputValueInContactTable("City", value, type);
        return this;
    }

    public InfoPage setRegion(String value, ContactType type) {
        setInputValueInContactTable("State/Province", value, type);
        return this;
    }

    public InfoPage setPostalCode(String value, ContactType type) {
        setInputValueInContactTable("Postal Code", value, type);
        return this;
    }

    public InfoPage setPhone(String value) {
        setInputValueInContactTable("Phone", value, ContactType.BILLING_CONTACT);
        return this;
    }

    public InfoPage setCountry(String value, ContactType type) {
        selectDropdownValueInContactTable("Country", value, type);
        return this;
    }

    public InfoPage setEmail(String value) {
        WebDriverHelper.waitForElementVisibility(driver, emailInput, this.TIMEOUT);
        emailInput.clear();
        emailInput.sendKeys(value);
        return this;
    }

    public FinalPage clickPurchase() {
        WebDriverHelper.waitForElementVisibility(driver, purchaseButton, this.PAGE_LOADING_TIMEOUT);
        purchaseButton.click();
        FinalPage finalPage = new FinalPage(driver);
        finalPage.waitForPage();
        return finalPage;
    }

}
