package opm.pages.product;

import opm.Tab;
import org.openqa.selenium.WebDriver;

public class iPadsPage extends ProductsAbstractPage {

    public iPadsPage(WebDriver driver) {
        super(driver, Tab.IPADS);
    }
}
