package opm.pages.product;

import opm.Tab;
import org.openqa.selenium.WebDriver;

public class iPodsPage extends ProductsAbstractPage {

    public iPodsPage(WebDriver driver) {
        super(driver, Tab.IPODS);
    }
}
