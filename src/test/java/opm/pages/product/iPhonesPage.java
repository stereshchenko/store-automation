package opm.pages.product;

import opm.Tab;
import org.openqa.selenium.WebDriver;

public class iPhonesPage extends ProductsAbstractPage {

    public iPhonesPage(WebDriver driver) {
        super(driver, Tab.IPHONES);
    }
}
