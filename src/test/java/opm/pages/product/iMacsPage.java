package opm.pages.product;

import opm.Tab;
import org.openqa.selenium.WebDriver;

public class iMacsPage extends ProductsAbstractPage {

    public iMacsPage(WebDriver driver) {
        super(driver, Tab.IMACS);
    }
}
