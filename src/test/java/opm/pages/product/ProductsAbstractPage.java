package opm.pages.product;

import opm.Tab;
import opm.pages.MainHeaderAbstractPage;
import opm.product.IProduct;
import opm.product.Product;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import utils.WebDriverHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class ProductsAbstractPage extends MainHeaderAbstractPage {
    protected final String headerXpath = "//header[contains(@class,'page-header')]//h1[text()='%s']";
    protected final String productsDivsXpath = "//div[contains(@class,'default_product_display') or contains(@class,'product_grid_item')]";
    protected final String productDivXpath = productsDivsXpath + "[descendant::a[text()='%s']]";
    protected final String productCurrentPriceXpath = productDivXpath + "//span[contains(@class,'currentprice')]";
    protected final String productAddToCartXpath = productDivXpath + "//input[@name='Buy']";
    protected final String productLoadingImgXpath = productDivXpath + "//div[contains(@style,'visibility')]//img[@title='Loading']";
    protected Tab tab;

    protected ProductsAbstractPage(WebDriver driver, Tab tab) {
        super(driver);
        this.tab = tab;
    }

    public void waitForPage() {
        WebDriverHelper.findElementByXpath(driver, String.format(this.headerXpath, tab.getName()), this.PAGE_LOADING_TIMEOUT);
    }

    public Product getProduct(String productTitle) {
        Product product = new Product(productTitle);
        product.setCurrentPrice(getCurrentPriceForProduct(product));
        return product;
    }

    public List<IProduct> getProducts(Collection<String> productTitles) {
        List<IProduct> products = new ArrayList<>();
        productTitles.forEach((String name) -> products.add(this.getProduct(name)));
        return products;
    }

    protected String getCurrentPriceForProduct(IProduct product) {
        return WebDriverHelper.findElementByXpath(driver, String.format(this.productCurrentPriceXpath, product.name()),
                this.ELEMENT_APPEAR_TIMEOUT).getText();
    }

    public void addToCart(IProduct product, Integer quantity) {
        for (int i = 0; i < quantity; i++)
            addToCart(product);
    }

    public void addToCart(IProduct product) {
        WebDriverHelper.findElementByXpath(driver, String.format(this.productAddToCartXpath, product.name()),
                this.ELEMENT_APPEAR_TIMEOUT).click();
        try {
            WebDriverHelper.waitForElementInVisibility(driver, WebDriverHelper.findElementByXpath(driver,
                    String.format(this.productLoadingImgXpath, product.name()), this.TIMEOUT), 2 * this.ELEMENT_APPEAR_TIMEOUT);
        } catch (NoSuchElementException ignore) {
            LOG.warning("Loading image did not appear after Add To Cart click!");
        }
    }

}