package opm.pages;

import opm.Tab;
import opm.pages.checkout.CartPage;
import opm.pages.product.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.WebDriverHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public abstract class MainHeaderAbstractPage extends AbstractPage {
    protected final String mainMenuXpath = "//ul[@id='menu-main-menu']";
    private final String tabXpath = mainMenuXpath + "//a[text()='%s']";

    @FindBy(xpath = "//*[@id='header_cart']//a[@title='Checkout']")
    protected WebElement cart;

    protected MainHeaderAbstractPage(WebDriver driver) {
        super(driver);
    }

    public MainHeaderAbstractPage switchOnTab(Tab tab) {
        Map<Tab, Supplier<MainHeaderAbstractPage>> tabs = new HashMap<Tab, Supplier<MainHeaderAbstractPage>>() {{
            put(Tab.HOME, () -> new HomePage(driver));
            put(Tab.ACCESSORIES, () -> new AccessoriesPage(driver));
            put(Tab.IMACS, () -> new iMacsPage(driver));
            put(Tab.IPADS, () -> new iPadsPage(driver));
            put(Tab.IPHONES, () -> new iPhonesPage(driver));
            put(Tab.IPODS, () -> new iPodsPage(driver));
            put(Tab.MACBOOKS, () -> new MacBooksPage(driver));
        }};
        moveMouseToTab(tab);
        WebDriverHelper.findElementByXpath(driver, String.format(tabXpath, tab.getName()), this.PAGE_LOADING_TIMEOUT).click();
        MainHeaderAbstractPage page = tabs.get(tab).get();
        page.waitForPage();
        return page;
    }

    private void moveMouseToTab(Tab tab) {
        if (tab.getParentTab() == null) {
            WebDriverHelper.moveToElement(driver,
                    WebDriverHelper.findElementByXpath(driver, String.format(tabXpath, tab.getName()), this.ELEMENT_APPEAR_TIMEOUT));
        } else
            moveMouseToTab(tab.getParentTab());
    }

    public CartPage navigateToCart() {
        cart.click();
        CartPage cartPage = new CartPage(driver);
        cartPage.waitForPage();
        return cartPage;
    }

}
