package opm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.StoreProperties;
import utils.WebDriverHelper;

public class HomePage extends MainHeaderAbstractPage {
    private final String url = StoreProperties.URL;

    @FindBy(xpath = mainMenuXpath + "/li[contains(@class,'current_page_item')]//a[text()='Home']")
    protected WebElement selectedHomeTab;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage navigateTo() {
        LOG.info("Try to navigate on Home page:" + this.url);
        driver.get(url);
        waitForPage();
        return this;
    }

    public void waitForPage() {
        WebDriverHelper.waitForElementVisibility(driver, this.selectedHomeTab, this.PAGE_LOADING_TIMEOUT);
    }
}
