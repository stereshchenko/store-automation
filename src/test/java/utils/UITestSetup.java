package utils;

import opm.browser.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

import java.util.logging.Logger;


public abstract class UITestSetup {

    private static WebDriver DRIVER;
    protected final Logger LOG = Logger.getLogger(UITestSetup.class.getName());
    protected AssertHelper<SoftAssert> assertHelper;
    protected SoftAssert softAssert = new SoftAssert();

    public static WebDriver getDRIVER() {
        return DRIVER;
    }

    public static void setDRIVER(WebDriver newDriverValue) {
        DRIVER = newDriverValue;
    }

    @BeforeMethod
    public void setupTest() {
        assertHelper = new AssertHelper(softAssert);
        DRIVER = WebDriverManager.getDriverInstance();
    }

    @AfterMethod
    public void tearDownTest() {
        closeBrowser();
    }

    private void closeBrowser() {
        DRIVER.close();
        DRIVER = null;
        WebDriverManager.setNull();
    }
}
